#include <stdio.h>

int main(void){
	//Declaro variables
	float nota;
	float bono;
	float notaFinal;

	// CASO 1
	nota = 2.999999; // Nota: perdiendo la materia.
	bono = 1e-7; // Bono de 0.0000001
	notaFinal = nota + bono; // ERROR: PRECISIÓN DE PUNTO FLOTANTE.

	// Imprimo Valores de CASO 1
	printf("\n%s\n", "### CASO 1 ###" );
	printf("Nota: %15.7f\n", nota);
	printf("Bono: %15.7f\n", bono);
	printf("Nota final: %.7f\n", notaFinal);

	//Comparo valores de nota y notaFinal.
	printf("¿Hay diferencia entre nota y notaFinal?: ");
	// Ver ADVERTENCIA
	(nota != notaFinal) ? printf("sí.\n") : printf("no.\n"); 

	// CASO 2
	nota = 2.999999; // Nota: perdiendo la materia.
	bono = 1e-6; // Bono de 0.000001
	notaFinal = nota + bono; // Nota final incluye bono.

	// Imprimo Valores de CASO 1
	printf("\n%s\n", "### CASO 2 ###" );
	printf("Nota: %15.7f\n", nota);
	printf("Bono: %15.7f\n", bono);
	printf("Nota final: %.7f\n", notaFinal);

	//Comparo valores de nota y notaFinal
	printf("¿Hay diferencia entre nota y notaFinal?: ");
	// Ver ADVERTENCIA
	(nota != notaFinal) ? printf("sí.\n") : printf("no.\n"); 

	// ADVERTENCIA: no usar operadores de igualdad (==) sobre valores de
	// punto flotante.
	
	return 0;
}
